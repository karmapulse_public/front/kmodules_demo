const webpack = require('webpack');
const path = require('path');

module.exports = {
    resolve: {
        modules: [path.join(__dirname, '../src'), 'node_modules'],
        alias: {
            icons: path.resolve(__dirname, '../src/common/icons/'),
            components: path.resolve(__dirname, '../src/common/components/'),
            visualizations: path.resolve(__dirname, '../src/visualizations')
        },
        extensions: ['.jsx', '.js', '.json'],
    }
}
