/*
*   MÓDULO PRINCIPAL
*  --------------------------
*   Este componente se encarga recibir la data
*   y renderear la visualización requerida
*   con la configuración que le haya llegado por props.
*
*   Tiene un "reset" que se aplica a todos los hijos a partir de este div.
*/

import React from 'react';
import {
    Error,
    Empty,
    VisName1,
    VisName2,
    VisName1Loading,
    VisName2Loading
} from 'visualizations';

import styles from './styles';

export default () => (
    <div {...styles()}>
        <h1>Soy el módulo principal.</h1>
        Visualizaciones:
        <VisName1 />
        <VisName1Loading />
        <hr />
        <VisName2 />
        <VisName2Loading />
        <hr />
        Cuando no hay data:
        <Empty />
        Cuando hubo un error:
        <Error />
        Prueba con archivo env:
        {process.env.HI}
    </div>
);
