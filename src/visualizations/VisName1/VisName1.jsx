/*
*   VISUALIZACIÓN 1
*  --------------------------
*   Este componente renderea la data que le ha llegado.
*   En la mayoría de los casos, es un componente stateless.
*/

import React from 'react';
import { Title } from 'components';
import formatBodyText from 'kmodules_helpers/formatBodyText';

import styles from './styles';

const VisName1 = () => (
    <div {...styles('plum', 'center')}>
        Soy la primer visualización del módulo.
        Aquí es donde despliego la data que me llega.
        Puedo llamar componentes:
        <Title text="Soy el título de la visualización 1" />
        Este es un test de los helpers:
        <div>{formatBodyText('@SusCasasola es la mejor del mundo')}</div>
    </div>
);

export default VisName1;
