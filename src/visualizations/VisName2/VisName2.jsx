/*
*   VISUALIZACIÓN 2
*  --------------------------
*   Este componente renderea la data que le ha llegado.
*   En la mayoría de los casos, es un componente stateless.
*/

import React from 'react';
import { Title } from 'components';

import styles from './styles';

export default () => (
    <div {...styles('deepskyblue', 'center')}>
        Soy la segunda visualización del módulo.
        Aquí es donde despliego la data que me llega.
        Puedo llamar componentes:
        <Title text="Soy el título de la Visualización 2" />
    </div>
);
