/*
*   VISUALIZACIÓN DE ERROR
*  --------------------------
*   Usar este componente cuando haya un error
*/

import React from 'react';
import styles from './styles';

export default () => (
    <div {...styles('red', 'center')}>
        Uuups :( hubo un error
    </div>
);
