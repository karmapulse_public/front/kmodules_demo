import React from 'react';
import { render } from 'react-dom';
import MainModule from './mainModule';

const MyApp = () => <MainModule />;

render(<MyApp />, document.getElementById('root'));
