import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    color: PropTypes.string
};

const defaultProps = {
    color: '#FFF'
};

const LikeIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14" viewBox="0 0 17 14">
        <path fill={props.color} fillRule="evenodd" d="M16.46 4.847c0 1.258-.719 2.22-.719 2.22a18.396 18.396 0 0 1-1.582 1.8l-4.752 4.616a1.251 1.251 0 0 1-1.728 0L2.927 8.866a18.392 18.392 0 0 1-1.582-1.799s-.72-.962-.72-2.22c0-2.387 1.992-4.32 4.448-4.32 1.264 0 2.404.51 3.214 1.333l.09.091a.214.214 0 0 0 .333 0l.09-.091A4.497 4.497 0 0 1 12.012.526c2.456 0 4.447 1.934 4.447 4.32"/>
    </svg>
    /* eslint-enable */
);

LikeIcon.propTypes = propTypes;
LikeIcon.defaultProps = defaultProps;

export default LikeIcon;
