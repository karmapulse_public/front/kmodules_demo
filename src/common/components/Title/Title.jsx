/*
*   COMPONENTE REUTILIZABLE
*  --------------------------
*   Usar este componente en varias visualizaciones si se requiere.
*/

import React from 'react';
import PropTypes from 'prop-types';
import { TwitterIcon } from 'icons';

import styles from './styles';

const propTypes = {
    text: PropTypes.string
};

const defaultProps = {
    text: 'Soy un título'
};

const Title = props => (
    <div {...styles('pink', 'center')}>
        <TwitterIcon color="pink" />
        <h1>{props.text}</h1>
    </div>
);

Title.propTypes = propTypes;
Title.defaultProps = defaultProps;

export default Title;
