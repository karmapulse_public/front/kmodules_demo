/*
*   COMPONENTE REUTILIZABLE
*  --------------------------
*   Usar este componente en varias visualizaciones si se requiere.
*/

import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles';

const propTypes = {
    text: PropTypes.string
};

const defaultProps = {
    text: 'Soy un header'
};

const Header = props => (
    <header {...styles('pink', 'center')}>
        <h1>{props.text}</h1>
    </header>
);

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;
